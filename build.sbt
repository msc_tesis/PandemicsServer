lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    name := """Pandemics Stats Generator""",
    organization := "com.example",
    version := "1.0-SNAPSHOT",
    crossScalaVersions := Seq( "3.3.1"),
    scalaVersion := crossScalaVersions.value.head,
    libraryDependencies ++= Seq(
      guice,
      "com.lihaoyi" %% "upickle" % "3.2.0", // SBT
      "org.scalatestplus.play" %% "scalatestplus-play" % "7.0.1" % Test
    )
  )
