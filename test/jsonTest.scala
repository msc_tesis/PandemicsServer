
import controllers.ToJSON
import org.scalatestplus.play.*
import play.api.libs.json._
import controllers.StatsGenerator
import scala.collection.mutable


class jsonTest extends PlaySpec {
  "JSON Test" should {
    "Check JSON" in {
      val jsonOb = mutable.Map[String, Int]()
      jsonOb += ("x" -> 1)
      val right = 1
      assert(jsonOb("x") == right)
      Json.toJson(jsonOb).toString must include("{\"x\":1}")

      val any = mutable.Map[String, Any]()
      any+=("hello"->"world")
      any+=("data"->jsonOb)
      val jsonObject: JsValue = ToJSON.toJSON(any);
      println(jsonObject.toString)
      
      
      println(ToJSON.toJSON(StatsGenerator.simulationTrustStats(2,1).asInstanceOf[Array[Any]]))
    }
  }
}