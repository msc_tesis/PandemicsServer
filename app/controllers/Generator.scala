package controllers

trait Generator {
  def simulationTrustStats(size: Int, influencerCount: Int): Array[collection.Map[String, Any]]
}
