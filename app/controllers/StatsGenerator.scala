package controllers

import scala.collection.mutable

object StatsGenerator  extends Generator{
  private val rand = new scala.util.Random

  private var curr: Int = 1

  private def nextTrustStats: Int = {
    rand.nextInt(101)
  }

  private def createInfluencerList(me:Int, size: Int, influencerCount: Int): Array[Any] = {
    val influences = new Array[Any](influencerCount)
    val influencerList = mutable.Set[Int]()
    influencerList += me
    for (i <- 0 until influencerCount){
      var by = rand.nextInt(size)
      while (by == me || (influencerList contains by)) {
        by = rand.nextInt(size)
      }
      influencerList += by
      val influencesMap = mutable.Map[String, Double]()
      influencesMap += "agent" ->by
      influencesMap += "power" -> rand.nextDouble()
      influences(i) = influencesMap
    }
    influences
  }

  private def agentTrustStats(me:Int, size: Int, influencerCount: Int): collection.Map[String, Any] = {
    val myMap = mutable.Map[String, Any]()
    myMap += ("mask" -> nextTrustStats)
    myMap += ("amulet" -> nextTrustStats)
    myMap += ("juice" -> nextTrustStats)
    myMap += ("influences" -> createInfluencerList(me, size, influencerCount))
    myMap += ("activeReinforce" -> 10)
    myMap += ("inactiveReinforce" -> 1)
    myMap
  }

  def simulationTrustStats(size: Int, influencerCount: Int): Array[collection.Map[String, Any]] = {
    val simulationTrustOutput = new Array[collection.Map[String, Any]](size)
    for (i <- 0 until size) {
      simulationTrustOutput(i) = agentTrustStats(i, size, influencerCount)
    }
    simulationTrustOutput
  }

}