package controllers

import play.api.*
import play.api.libs.Files
import play.api.mvc.*
import upickle.default.*

import java.io.FileOutputStream
import javax.inject.*

/**
 *
 *
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */

  def a(): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] =>
      Ok("abc " + request.queryString("x").head)

  }

  def stats(): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] =>
      Ok(ToJSON.toJSON(StatsGenerator.simulationTrustStats(
        request.queryString("size").head.toInt,
        request.queryString("influencerCount").head.toInt
      ).asInstanceOf[Array[Any]]))
  }

  def results(): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] =>
      val a: Option[MultipartFormData[Files.TemporaryFile]] = request.body.asMultipartFormData
      val data: MultipartFormData[Files.TemporaryFile] = a.get
      val dataParts: Map[String, Seq[String]] = data.dataParts

      println(dataParts)
      val framework: String = dataParts("framework").head
      val influencers: String = dataParts("influencers").head
      val sickRatio: Seq[Int] = read[Seq[Int]](dataParts("sickRatio").head)
      val juiceTrust: Seq[Int] = read[Seq[Int]](dataParts("juiceTrust").head)
      val amuletTrust: Seq[Int] = read[Seq[Int]](dataParts("amuletTrust").head)
      val maskTrust: Seq[Int] = read[Seq[Int]](dataParts("maskTrust").head)

      println(framework + "." + influencers + ".csv")
      val fos: FileOutputStream = new FileOutputStream(framework + "." + influencers + ".csv")
      fos.write("Sick Ratio, Juice Trust, Amulet Trust, Mask Trust\n".getBytes)
      for (i: Int <- sickRatio.indices) {
        fos.write((sickRatio(i).toString + ",").getBytes)
        fos.write((juiceTrust(i).toString + ",").getBytes)
        fos.write((amuletTrust(i).toString + ",").getBytes)
        fos.write(maskTrust(i).toString.getBytes)
        fos.write("\n".getBytes)
      }
      fos.close()

      /*println("a.get" + a.get.getClass)
      println("a.size" + a.size)
      println("data.dataParts" + data.dataParts)
      println("data.dataParts" + data.files)
      for ((a: String, b: Seq[String]) <- data.dataParts) {
        println("name " + a + ". value " + b(0))
      }*/

      Ok("abc " + request.body)

  }

}
