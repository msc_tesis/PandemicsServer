package controllers

import scala.collection.mutable

object StatsGeneratorPromote extends Generator {
  private val rand = new scala.util.Random

  private var curr: Int = 1

  private def nextTrustStats: Int = {
    rand.nextInt(101)
  }

  private def agentTrustStats(me:Int, size: Int, influencerCount: Int): collection.Map[String, Any] = {
    val myMap = mutable.Map[String, Any]()
    myMap += ("mask" -> 0)
    myMap += ("amulet" -> 100)
    myMap += ("juice" -> nextTrustStats)
    myMap += ("influences" -> new Array[String](0))
    myMap += ("activeReinforce" -> 0)
    myMap += ("inactiveReinforce" -> 0)

  }

  def simulationTrustStats(size: Int, influencerCount: Int): Array[collection.Map[String, Any]] = {
    val simulationTrustOutput = new Array[collection.Map[String, Any]](size)
    for (i <- 0 until size) {
      simulationTrustOutput(i) = agentTrustStats(i, size, influencerCount)
    }
    simulationTrustOutput
  }

}