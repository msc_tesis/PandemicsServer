package controllers

import play.api.libs.json.*

import scala.collection
import scala.collection.mutable


object ToJSON {
  def toJSON(inputMap: Array[ Any]): JsValue = {
    var outputArray = new Array[JsValue](inputMap.length)
    for (i <- inputMap.indices){
      val value = inputMap(i)
      value match
        case str: String =>outputArray(i)= JsString(str)
        case d: Double => outputArray(i)=JsNumber(d)
        case i: Int => outputArray(i)=JsNumber(i)
        case bool: Boolean =>  outputArray(i)=JsBoolean(bool)
        case childMap: collection.Map[String, Any] => outputArray(i)= toJSON(childMap)
        case _ =>outputArray(i)= JsNull
    }
     JsArray(outputArray)
  }

  def toJSON(inputMap: collection.Map[String, Any]): JsValue = {
    val outputMap: mutable.Map[String, JsValue] = mutable.Map[String, JsValue]()
    inputMap.foreach { case (key, value) =>
      value match
        case str: String =>
          outputMap += (key -> JsString(str))
        case d: Double =>
          outputMap += (key -> JsNumber(d))
        case i: Int =>
          outputMap += (key -> JsNumber(i))
        case bool: Boolean =>
          outputMap += (key -> JsBoolean(bool))
        case childMap: collection.Map[String, Any] =>
          outputMap += (key -> toJSON(childMap))
        case childArray: Array[ Any] =>
          outputMap += (key -> toJSON(childArray))
        case _ =>
    }
     new JsObject(outputMap)
  }
}
